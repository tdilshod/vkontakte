using System;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.VKEditor;
using UnityEditor.XCodeEditor;

namespace UnityEditor.VKEditor {
    public static class XCodePostProcess {
		const string VKPath = "VK";

		[PostProcessBuild(100)]
        public static void OnPostProcessBuild(BuildTarget target, string path) {
            // If integrating with vk on any platform, throw a warning if the app id is invalid
            if (!VKSettings.IsValidAppId)
                Debug.LogWarning("You didn't specify a VK app ID.");

            if (target == BuildTarget.iPhone) {
                UnityEditor.XCodeEditor.XCProject project = new UnityEditor.XCodeEditor.XCProject(path);

                // Find and run through all projmods files to patch the project

                string projModPath = System.IO.Path.Combine(Application.dataPath, VKPath + "/Editor/iOS");
                var files = System.IO.Directory.GetFiles(projModPath, "*.projmods", System.IO.SearchOption.AllDirectories);
                foreach (var file in files)
                {
					project.ApplyMod(Application.dataPath, file);
				}
                project.Save();

                UpdatePlist(path);
                //FixupFiles.FixSimulator(path);
                FixupFiles.AddVersionDefine(path);
                FixupFiles.FixColdStart(path);
            }

            if (target == BuildTarget.Android) {
                // The default Bundle Identifier for Unity does magical things that causes bad stuff to happen
                if (PlayerSettings.bundleIdentifier == "com.Company.ProductName")
                    Debug.LogError("The default Unity Bundle Identifier (com.Company.ProductName) will not work correctly.");
                if (!VKAndroidUtil.IsSetupProperly())
                    Debug.LogError("Your Android setup is not correct. See Settings in VK menu.");
                if (!ManifestMod.CheckManifest())
                    // If something is wrong with the Android Manifest, try to regenerate it to fix it for the next build.
                    ManifestMod.GenerateManifest();
            }

			if (target == BuildTarget.WebPlayer || target == BuildTarget.WebPlayerStreamed) {
				FileUtil.ReplaceFile("Assets/" + VKPath + "/Editor/webplayer/vk.js.txt", path + "/vk.js");
				string data = System.IO.File.ReadAllText("Assets/" + VKPath + "/Editor/webplayer/index.html");
				data = data.Replace("[%name%]", System.IO.Path.GetFileName(path));
				System.IO.File.WriteAllText(path + "/index.html", data);
			}
        }

        public static void UpdatePlist(string path) {
            const string fileName = "Info.plist";
            string appId = VKSettings.AppId;
            string fullPath = Path.Combine(path, fileName);

            if (string.IsNullOrEmpty(appId) || appId.Equals("0")) {
                Debug.LogError("You didn't specify a VK app ID.");
                return;
            }

            var vkParser = new VKPListParser(fullPath);
            vkParser.UpdateVKSettings(appId, VKSettings.AllAppIds);
            vkParser.WriteToFile();
        }
    }
}
