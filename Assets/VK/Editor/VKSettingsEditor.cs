//using VK;
using UnityEngine;
using UnityEditor;
using UnityEditor.VKEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

[CustomEditor(typeof(VKSettings))]
public class VKSettingsEditor : Editor {
    bool showAndroidUtils = (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android);
    bool showIOSSettings = (EditorUserBuildSettings.activeBuildTarget == BuildTarget.iPhone);

	GUIContent appNameLabel = new GUIContent("App Name [?]:", "For your own use and organization.\n(ex. 'dev', 'qa', 'prod')");
	GUIContent appIdLabel = new GUIContent("App Id [?]:", "VK App Ids can be found at https://vk.com/apps?act=manage");

	GUIContent urlSuffixLabel = new GUIContent ("URL Scheme Suffix [?]", "Use this to share VK APP ID's across multiple iOS apps.  https://vk.com/apps?act=manage");
    GUIContent packageNameLabel = new GUIContent("Package Name [?]", "aka: the bundle identifier");
    GUIContent debugAndroidKeyLabel = new GUIContent("Debug Android Key Hash [?]", "Copy this key to the VK Settings in order to test a VK Android app");
	GUIContent androidKeyLabel = new GUIContent("Android Key Hash [?]", "Copy this key to the VK Settings in order to test a VK Android app");

    private VKSettings instance;

	void OnEnable() {
		VKAndroidUtil.ClearKeyHash();
	}

    public override void OnInspectorGUI() {
        instance = (VKSettings)target;

        AppIdGUI();
        AndroidUtilGUI();
        IOSUtilGUI();
    }

    private void AppIdGUI() {
        EditorGUILayout.HelpBox("1) Set the VK App Id associated with this game", MessageType.None);
        if (instance.AppIds.Length == 0 || instance.AppIds[instance.SelectedAppIndex] == "0")
            EditorGUILayout.HelpBox("Invalid App Id", MessageType.Error);

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(appNameLabel);
		EditorGUILayout.LabelField(appIdLabel);
        EditorGUILayout.EndHorizontal();
        for (int i = 0; i < instance.AppIds.Length; ++i) {
            EditorGUILayout.BeginHorizontal();
			instance.SetAppLabel(i, EditorGUILayout.TextField(instance.AppLabels[i]));
			GUI.changed = false;
            instance.SetAppId(i, EditorGUILayout.TextField(instance.AppIds[i]));
            if (GUI.changed)
                ManifestMod.GenerateManifest();
            EditorGUILayout.EndHorizontal();
        }
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Add Another App Id"))
		{
			var appLabels = new List<string>(instance.AppLabels);
			appLabels.Add("New App");
			instance.AppLabels = appLabels.ToArray();
			
			var appIds = new List<string>(instance.AppIds);
			appIds.Add("0");
			instance.AppIds = appIds.ToArray();
		}
		if (instance.AppLabels.Length > 1)
		{
			if (GUILayout.Button("Remove Last App Id"))
			{
				var appLabels = new List<string>(instance.AppLabels);
				appLabels.RemoveAt(appLabels.Count - 1);
				instance.AppLabels = appLabels.ToArray();
				
				var appIds = new List<string>(instance.AppIds);
				appIds.RemoveAt(appIds.Count - 1);
				instance.AppIds = appIds.ToArray();
			}
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();
        if (instance.AppIds.Length > 1) {
            EditorGUILayout.HelpBox("2) Select VK App Id to be compiled with this game", MessageType.None);
            GUI.changed = false;
            instance.SetAppIndex(EditorGUILayout.Popup("Selected App Id", instance.SelectedAppIndex, instance.AppLabels));
            if (GUI.changed)
                ManifestMod.GenerateManifest();
            EditorGUILayout.Space();
        } else {
            instance.SetAppIndex(0);
        }
    }

    private void IOSUtilGUI() {
        showIOSSettings = EditorGUILayout.Foldout(showIOSSettings, "iOS Build Settings");
        if (showIOSSettings) {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(urlSuffixLabel, GUILayout.Width(135), GUILayout.Height(16));
			VKSettings.IosURLSuffix = EditorGUILayout.TextField(VKSettings.IosURLSuffix);
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.Space();
    }

    private void AndroidUtilGUI() {
        showAndroidUtils = EditorGUILayout.Foldout(showAndroidUtils, "Android Build VK Settings");
        if (showAndroidUtils) {
            if (!VKAndroidUtil.IsSetupProperly()) {
                var msg = "Your Android setup is not right. Check the documentation.";
				switch (VKAndroidUtil.SetupError) {
					case VKAndroidUtil.ERROR_NO_SDK:
                        msg = "You don't have the Android SDK setup!  Go to " + (Application.platform == RuntimePlatform.OSXEditor ? "Unity" : "Edit") + "->Preferences... and set your Android SDK Location under External Tools";
                        break;
					case VKAndroidUtil.ERROR_NO_KEYSTORE:
                        msg = "Your android debug keystore file is missing! You can create new one by creating and building empty Android project in Ecplise.";
                        break;
					case VKAndroidUtil.ERROR_NO_KEYTOOL:
                        msg = "Keytool not found. Make sure that Java is installed, and that Java tools are in your path.";
                        break;
					case VKAndroidUtil.ERROR_NO_OPENSSL:
                        msg = "OpenSSL not found. Make sure that OpenSSL is installed, and that it is in your path.";
                        break;
					case VKAndroidUtil.ERROR_KEYTOOL_ERROR:
                        msg = "Unkown error while getting Debug Android Key Hash.";
                        break;
                }
                EditorGUILayout.HelpBox(msg, MessageType.Warning);
            }
            EditorGUILayout.HelpBox("Copy and Paste these into your \"Native Android App\" Settings on developers.facebook.com/apps", MessageType.None);
            SelectableLabelField(packageNameLabel, PlayerSettings.bundleIdentifier);

			string debugHashKey = VKAndroidUtil.DebugKeyHash;
			// decode to base64 and encode it to hex
			if (debugHashKey != null)
				debugHashKey = System.BitConverter.ToString(System.Convert.FromBase64String(debugHashKey)).Replace("-", "");

			SelectableLabelField(debugAndroidKeyLabel, debugHashKey);

			if (!string.IsNullOrEmpty(PlayerSettings.Android.keystoreName) && !string.IsNullOrEmpty(PlayerSettings.Android.keystorePass)) {
				string hashKey = VKAndroidUtil.KeyHash;
				if (hashKey != null)
					hashKey = System.BitConverter.ToString(System.Convert.FromBase64String(hashKey)).Replace("-", "");
				SelectableLabelField(androidKeyLabel, hashKey);
			}

            if (GUILayout.Button("Regenerate Android Manifest"))
                ManifestMod.GenerateManifest();
        }
        EditorGUILayout.Space();
    }

    private void SelectableLabelField(GUIContent label, string value) {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, GUILayout.Width(180), GUILayout.Height(16));
        EditorGUILayout.SelectableLabel(value, GUILayout.Height(16));
        EditorGUILayout.EndHorizontal();
    }
}
