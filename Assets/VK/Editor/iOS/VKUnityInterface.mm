//
//  VKUnityInterface.mm
//  Unity-iPhone
//

#include "VKUnityInterface.h"
#import <VKSdk.h>
#import <Foundation/NSJSONSerialization.h>
#include <string>

static VKUnityInterface *_instance = [VKUnityInterface sharedInstance];
const char *g_vkObjName = "UnityVKSDKPlugin";

extern "C" void iosGetDeepLink();

@implementation VKUnityInterface

+(VKUnityInterface *)sharedInstance {
  return _instance;
}

+ (void)initialize {
  if (!_instance)
    _instance = [[VKUnityInterface alloc] init];
}

- (id)init {
  if (_instance != nil)
    return _instance;

  self = [super init];
  if (!self)
    return nil;

  _instance = self;

  self.isInitializing = YES;

  [[NSNotificationCenter defaultCenter]
   addObserver:self
   selector:@selector(didBecomeActive:)
   name:UIApplicationDidBecomeActiveNotification
   object:nil];

  [[NSNotificationCenter defaultCenter]
   addObserver:self
   selector:@selector(willTerminate:)
   name:UIApplicationWillTerminateNotification
   object:nil];

  [[NSNotificationCenter defaultCenter]
   addObserver:self
   selector:@selector(didFinishLaunching:)
   name:UIApplicationDidFinishLaunchingNotification
   object:nil];

#if UNITY_VERSION >= 430
  UnityRegisterAppDelegateListener(self);
#endif
  return self;
}

- (id)initWithAppId:(const char *)_appId {
    self = [self init];
    
    [VKSdk initializeWithDelegate:(id<VKSdkDelegate>)self andAppId:[NSString stringWithUTF8String:_appId]];
    if ([VKSdk wakeUpSession] && [VKSdk getAccessToken]) {
        VKAccessToken* accessToken = [VKSdk getAccessToken];

        NSMutableDictionary *msgData = [NSMutableDictionary dictionary];
        [msgData setObject:accessToken.userId forKey:@"user_id"];
        [msgData setObject:accessToken.accessToken forKey:@"access_token"];
        [msgData setObject:accessToken.expiresIn forKey:@"expiration_timestamp"];

        [VKUnityInterface sendMessageToUnity:"OnInitComplete" userData:msgData];
    } else {
        UnitySendMessage(g_vkObjName, "OnInitComplete", "");
    }
    return self;
}

+ (void)sendMessageToUnity:(const char *)unityMessage userData:(NSDictionary *)userData
{
  NSError *serializationError = nil;
  NSData *jsonData = nil;
  if(userData != nil) {
    jsonData = [NSJSONSerialization dataWithJSONObject:userData options:0 error:&serializationError];
  }

  const char *userDataString = nil;
  NSString *jsonString = nil;
  if (jsonData) {
    jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    userDataString = [jsonString cStringUsingEncoding:NSUTF8StringEncoding];
  }

  UnitySendMessage(g_vkObjName, unityMessage, userDataString == nil ? "" : userDataString);
}

-(void)login:(const char *)cScope {
    NSString *scopeStr = [NSString stringWithUTF8String:cScope];
    NSArray *scope = nil;
    if (cScope && strlen(cScope) > 0)
      scope = [scopeStr componentsSeparatedByString:@","];

    [VKSdk authorize:scope];
}

-(void)logout {
    [VKSdk forceLogout];
    UnitySendMessage(g_vkObjName, "OnLogout", "");
}

-(void)didBecomeActive: (NSNotification *)notification {
}

-(void)willTerminate:(NSNotification *)notification {
}

-(void)didFinishLaunching:(NSNotification *)notification {
}

- (BOOL)openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication {
    NSString *urlString = [url absoluteString];
    [[NSUserDefaults standardUserDefaults] setObject:urlString forKey:@"vk-open-url"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    return [VKSdk processOpenURL:url fromApplication:sourceApplication];
}

- (NSDictionary *)parseQueryString:(NSString *)query {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [dict setObject:val forKey:key];
    }
    return dict;
}

#if UNITY_VERSION >= 430
- (void)onOpenURL:(NSNotification*)notification {
  [self openURL:[notification.userInfo objectForKey:@"url"] sourceApplication:[notification.userInfo objectForKey:@"sourceApplication"]];
}
#endif

#pragma mark - VKSdkDelegate
- (void) vkSdkRenewedToken:(VKAccessToken *)newToken {
    NSMutableDictionary *msgData = [NSMutableDictionary dictionary];
    [msgData setObject:newToken.userId forKey:@"user_id"];
    [msgData setObject:newToken.accessToken forKey:@"access_token"];
    [msgData setObject:newToken.expiresIn forKey:@"expiration_timestamp"];
    
    [VKUnityInterface sendMessageToUnity:"OnRenewToken" userData:msgData];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    UIViewController *viewController = UnityGetGLViewController();
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:viewController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
    [VKSdk authorize:nil];
    UnitySendMessage(g_vkObjName, "OnLogout", "");
}

- (void) vkSdkReceivedNewToken:(VKAccessToken *)newToken {
    NSMutableDictionary *msgData = [NSMutableDictionary dictionary];
    [msgData setObject:newToken.userId forKey:@"user_id"];
    [msgData setObject:newToken.accessToken forKey:@"access_token"];
    [msgData setObject:newToken.expiresIn forKey:@"expiration_timestamp"];

    [VKUnityInterface sendMessageToUnity:"OnLogin" userData:msgData];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    // skip one frame before show
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.33 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        UIViewController *viewController = UnityGetGLViewController();
        [viewController presentViewController:controller animated:NO completion:nil];
    });
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError {
    UnitySendMessage(g_vkObjName, "OnLogin", "");
}

@end


//everything in the extern "C" section is callable from C# unity
extern "C" {

void iosVKInit(const char *_appId) {
    [[VKUnityInterface alloc] initWithAppId: _appId];
}

void iosVKLogin(const char *scope) {
    [[VKUnityInterface sharedInstance] login:scope];
}

void iosVKLogout() {
    [[VKUnityInterface sharedInstance] logout];
}

void iosVKShareDialog(const char *title, const char *text, const char *cUrl, const char *imagePath) {
    NSString *url = NULL;
    if (cUrl && strlen(cUrl))
        url = [NSString stringWithUTF8String:cUrl];
    else
        url = @"";

    NSArray *images = NULL;
    if (imagePath && strlen(imagePath)) {
        NSString *filePath = [NSString stringWithUTF8String:imagePath];
        VKUploadImage *image = [VKUploadImage uploadImageWithImage:[UIImage imageWithContentsOfFile:filePath] andParams:[VKImageParameters jpegImageWithQuality:0.9]];
        images = [NSArray arrayWithObject:image];
    }

    VKShareDialogController * shareDialog = [VKShareDialogController new];
    shareDialog.text = [NSString stringWithUTF8String: text];
    shareDialog.shareLink = [[VKShareLink alloc] initWithTitle:[NSString stringWithUTF8String: title] link:[NSURL URLWithString: url]];
    if (images != NULL)
        [shareDialog setUploadImages:images];

    UIViewController* vc = UnityGetGLViewController();
    [shareDialog setCompletionHandler:^(VKShareDialogControllerResult result) {
        [vc dismissViewControllerAnimated:YES completion:nil];
    }];
    [vc presentViewController:shareDialog animated:YES completion:nil];
}

}
