
#import <UIKit/UIKit.h>

#include "RegisterMonoModules.h"

//if we are on a version of unity that has the version number defined use it, otherwise we have added it ourselves in the post build step
#if HAS_UNITY_VERSION_DEF
  #include "UnityTrampolineConfigure.h"
#endif


#import "AppDelegateListener.h"
@interface VKUnityInterface : NSObject <AppDelegateListener>

@property (nonatomic) BOOL isInitializing;

+(VKUnityInterface *)sharedInstance;
-(id)init;

#if UNITY_VERSION >= 430
- (void)onOpenURL:(NSNotification*)notification;
#endif
@end
