﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Example : MonoBehaviour {
	bool initialized = false;

	void Start() {
		VKApi.testMode = true;
		VKontakte.Init(() => {
			initialized = true;
		});
	}

	void OnGUI() {
		int height = 80;
		#if UNITY_WEBPLAYER
		height = 60;
		#endif

		if (initialized) {
			if (!VKontakte.IsLoggedIn && GUI.Button(new Rect(10, 10, 200, 100), "Login"))
				VKontakte.Login("friends,photos,wall,status,messages", (success) => {

				});

			if (VKontakte.IsLoggedIn) {
				GUILayout.BeginVertical();
				if (VKontakte.LogoutSupported() && GUILayout.Button("Logout", GUILayout.Width(200), GUILayout.Height(height)))
					VKontakte.Logout();

				if (VKontakte.ShareDialogSupported() && GUILayout.Button("Share Dialog", GUILayout.Width(200), GUILayout.Height(height)))
					VKontakte.ShareDialog("TItle", "TExt", "http://ya.ru", Application.streamingAssetsPath + "/a.jpg");

				if (GUILayout.Button("Post To Wall", GUILayout.Width(200), GUILayout.Height(height)))
					VKApi.PostToWall("Check it out!", "http://google.com");
				
				if (GUILayout.Button("Get Friends", GUILayout.Width(200), GUILayout.Height(height))) {
					VKApi.GetFriends((friends, error) => {
						if (error != null) {
							Debug.Log(error.errorCode);
							Debug.Log(error.errorMessage);
						} else {
							foreach (var f in friends) {
								Debug.Log(
									f.id + ": " + f.firstName + " " + f.lastName + (f.online ? " online" : " offline") + " " + f.photoUrl
								);
							}
						}
					});
				}

				if (GUILayout.Button("Get App Friends", GUILayout.Width(200), GUILayout.Height(height))) {
					VKApi.GetAppFriends((friends, error) => {
						if (error != null) {
							Debug.Log(error.errorCode);
							Debug.Log(error.errorMessage);
						} else {
							foreach (var f in friends) {
								Debug.Log(
									f.id + ": " + f.firstName + " " + f.lastName + (f.online ? " online" : " offline") + " " + f.photoUrl
								);
							}
						}
					});
				}

				if (!VKontakte.InviteBoxSupported() && GUILayout.Button("Send App Request", GUILayout.Width(200), GUILayout.Height(height))) {
					VKApi.AppSendInviteRequest(231901774, "Hello There", (id, error) => {
						if (error != null) {
							Debug.Log(error.errorCode);
							Debug.Log(error.errorMessage);
						} else
							Debug.Log ("Result is: " + id);
					});
				}

				if (VKontakte.InviteBoxSupported() && GUILayout.Button("Show Invite Box", GUILayout.Width(200), GUILayout.Height(height)))
					VKontakte.ShowInviteBox();

				if (VKontakte.OrderBoxSupported() && GUILayout.Button("Show Item Order Box", GUILayout.Width(200), GUILayout.Height(height)))
					VKontakte.ShowItemOrderBox("test_item", (s) => Debug.Log("Success: " + s), () => Debug.Log("Cancel"), () => Debug.Log("Failed"));

				if (VKontakte.OrderBoxSupported() && GUILayout.Button("Show Offer Order Box", GUILayout.Width(200), GUILayout.Height(height)))
					VKontakte.ShowOffersOrderBox(10, true, (s) => Debug.Log("Success: " + s), () => Debug.Log("Cancel"), () => Debug.Log("Failed"));

				if (VKontakte.OrderBoxSupported() && GUILayout.Button("Show Votes Order Box", GUILayout.Width(200), GUILayout.Height(height)))
					VKontakte.ShowVotesOrderBox(10, (s) => Debug.Log("Success: " + s), () => Debug.Log("Cancel"), () => Debug.Log("Failed"));

				if (GUILayout.Button("Send Message", GUILayout.Width(200), GUILayout.Height(height)))
					VKApi.SendMessageToFriend(new int[] {210656175}, "Вам подарок в приложении http://google.com\nСпасибо!");

				if (GUILayout.Button("Storage Test", GUILayout.Width(200), GUILayout.Height(height)))
					//StorageTest();
					BatchInviteTest();
			}
		}
	}

	void BatchInviteTest() {
		VKApi.SendBatchInviteRequests(new int[] {132570235, 8506688, 26047}, "Test", (result) => {

		});
	}

	void StorageTest() {
/*
		// Set value for key
		VKApi.StorageSet(false, 210656175, "testKey", "HelloWorld2", (error) => {
			if (error != null)
				Debug.Log (error);
		});
		VKApi.StorageSet(false, 210656175, "test2", "123", (error) => {
			if (error != null)
				Debug.Log (error);
		});

		// Get keys
		VKApi.StorageGetKeys(false, 210656175, (keys, error2) => {
			if (error2 != null)
				Debug.Log (error2);
			else
				foreach (string key in keys)
					Debug.Log (key);
		});

		// Set value for key
		VKApi.StorageGet(false, 210656175, "testKey", (value, error) => {
			if (error != null)
				Debug.Log (error);
			else
				Debug.Log (value);
		});
*/
		// Set values for keys
		VKApi.StorageGet(false, 210656175, new string[] {"testKey", "test2"}, (values, error) => {
			if (error != null)
				Debug.Log (error);
			else {
				foreach (KeyValuePair<string, string> e in values)
					Debug.Log (e.Key + " = " + e.Value);
			}
		});
	}
}
