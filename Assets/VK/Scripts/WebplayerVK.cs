﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace VK {
	class WebplayerVK : AbstractVK, IVK {
		private System.Action externalInitDelegate;

		#region Init
		protected override void OnAwake() {
		}
		
		public override void Init(System.Action onInitComplete, string appId) {
			externalInitDelegate = onInitComplete;
			Application.ExternalCall("VKInitialize");
		}
		#endregion
		
		public override void Login(string scope = "", System.Action<bool> callback = null) {
		}
		
		public override void Logout() {
		}
		
		public override bool ShareDialogSupported() {
			return true;
		}

		public override bool LogoutSupported() {
			return false;
		}

		public override bool PaymentsSupported() {
			return true;
		}

		public override bool InviteBoxSupported() {
			return true;
		}

		public override bool OrderBoxSupported() {
			return true;
		}

		System.Action<int> onOrderSuccess;
		System.Action onOrderFail, onOrderCancel;
		public override void ShowOrderBox(OrderType type, int votes, int offerId, bool currency, string item, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail) {
			this.onOrderSuccess = onSuccess;
			this.onOrderFail = onFail;
			this.onOrderCancel = onCancel;
			Application.ExternalCall("VKShowOrderBox", type.ToString(), item, votes, offerId, currency ? 1 : 0);
		}

		public override void ShowInviteBox() {
			Application.ExternalCall("VKShowInviteBox");
		}

		public override void ShareDialog(string title, string text, string url, string imagePath) {
		    Application.ExternalCall("VKPostToWall", text, url);
		}

		private void OnInitComplete(string msg) {
			this.isInitialized = true;
			if (!string.IsNullOrEmpty(msg))
				OnLogin(msg);
			
			externalInitDelegate();
		}

		public void OnLogin(string msg) {
			if (string.IsNullOrEmpty(msg)) {
				// cancelled
				return;
			}
			var parameters = JSON.JsonDecode(msg); //(Dictionary<string, object>)MiniJSON.Json.Deserialize(msg);
			if (parameters.ContainsKey ("user_id"))
				isLoggedIn = true;

			//pull userId, access token and expiration time out of the response
			ParseLoginDict (parameters);
		}

		public void OnRenewToken(string msg) {
			if (string.IsNullOrEmpty(msg)) {
				// error
				return;
			}
			var parameters = JSON.JsonDecode(msg);
			isLoggedIn = parameters.ContainsKey("user_id");
			
			//pull userId, access token and expiration time out of the response
			ParseLoginDict (parameters);
		}
		
		public void OnLogout() {
			accessToken = null;
			isLoggedIn = false;
		}

		public void OnOrderSuccess(string orderIdString) {
			if (onOrderSuccess != null) {
				int orderId = int.Parse(orderIdString);
				onOrderSuccess(orderId);
				onOrderSuccess = null;
				onOrderCancel = onOrderFail = null;
			}
		}

		public void OnOrderFail() {
			if (onOrderFail != null) {
				OnOrderFail();
				onOrderSuccess = null;
				onOrderCancel = onOrderFail = null;
			}
		}

		public void OnOrderCancel() {
			if (onOrderCancel != null) {
				OnOrderCancel();
				onOrderSuccess = null;
				onOrderCancel = onOrderFail = null;
			}
		}

		public void OnPostToWall(string msg) {
		    // TODO: finish it if you need to know if user posted to the wall or canceled
			/*if (string.IsNullOrEmpty(msg)) {
				// error
				return;
			}
			var parameters = JSON.JsonDecode(msg);
			if (parameters.ContainsKey("error")) {
			    // canceled or something else
			} else {
			    Hastable response = parameters["response"] as Hashtable;
			    int postId = (int)response["post_id"];
			    
			}*/
		}
	}
}
