using UnityEngine;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
#endif
public class VKSettings : ScriptableObject
{

    const string vkSettingsAssetName = "VKSettings";
    const string vkSettingsPath = "VK/Resources";
    const string vkSettingsAssetExtension = ".asset";

    private static VKSettings instance;

    static VKSettings Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load(vkSettingsAssetName) as VKSettings;
                if (instance == null)
                {
                    // If not found, autocreate the asset object.
                    instance = CreateInstance<VKSettings>();
#if UNITY_EDITOR
                    string properPath = Path.Combine(Application.dataPath, vkSettingsPath);
                    if (!Directory.Exists(properPath))
                    {
                        AssetDatabase.CreateFolder("Assets/VK", "Resources");
                    }

                    string fullPath = Path.Combine(Path.Combine("Assets", vkSettingsPath),
                                                   vkSettingsAssetName + vkSettingsAssetExtension
                                                  );
                    AssetDatabase.CreateAsset(instance, fullPath);
#endif
                }
            }
            return instance;
        }
    }

#if UNITY_EDITOR
    [MenuItem("VK/Edit Settings")]
    public static void Edit()
    {
        Selection.activeObject = Instance;
    }

    [MenuItem("VK/Developers Page")]
    public static void OpenAppPage()
    {
		string url = "https://vk.com/dev";
        if (Instance.AppIds[Instance.SelectedAppIndex] != "0")
            url = "https://vk.com/editapp?id=" + Instance.AppIds[Instance.SelectedAppIndex] + "&section=options";
        Application.OpenURL(url);
    }
#endif

    #region App Settings

    [SerializeField]
    private int selectedAppIndex = 0;
    [SerializeField]
    private string[] appIds = new[] { "0" };
    [SerializeField]
    private string[] appLabels = new[] { "App Name" };
    [SerializeField]
    private bool cookie = true;
    [SerializeField]
    private bool logging = true;
    [SerializeField]
    private bool status = true;
    [SerializeField]
    private bool xfbml = false;
    [SerializeField]
    private bool frictionlessRequests = true;
    [SerializeField]
    private string iosURLSuffix = "";

    public void SetAppIndex(int index)
    {
        if (selectedAppIndex != index)
        {
            selectedAppIndex = index;
            DirtyEditor();
        }
    }

    public int SelectedAppIndex
    {
        get { return selectedAppIndex; }
    }

    public void SetAppId(int index, string value)
    {
        if (appIds[index] != value)
        {
            appIds[index] = value;
            DirtyEditor();
        }
    }

    public string[] AppIds
    {
        get { return appIds; }
        set
        {
            if (appIds != value)
            {
                appIds = value;
                DirtyEditor();
            }
        }
    }

    public void SetAppLabel(int index, string value)
    {
        if (appLabels[index] != value)
        {
            AppLabels[index] = value;
            DirtyEditor();
        }
    }

    public string[] AppLabels
    {
        get { return appLabels; }
        set
        {
            if (appLabels != value)
            {
                appLabels = value;
                DirtyEditor();
            }
        }
    }

    public static string[] AllAppIds
    {
        get
        {
            return Instance.AppIds;
        }
    }

    public static string AppId
    {
        get
        {
            return Instance.AppIds[Instance.SelectedAppIndex];
        }
    }

    public static bool IsValidAppId
    {
        get
        {
            return VKSettings.AppId != null 
                && VKSettings.AppId.Length > 0 
                && !VKSettings.AppId.Equals("0");
        }
    }

    public static bool Cookie
    {
        get { return Instance.cookie; }
        set
        {
            if (Instance.cookie != value)
            {
                Instance.cookie = value;
                DirtyEditor();
            }
        }
    }

    public static bool Logging
    {
        get { return Instance.logging; }
        set
        {
            if (Instance.logging != value)
            {
                Instance.logging = value;
                DirtyEditor();
            }
        }
    }

    public static bool Status
    {
        get { return Instance.status; }
        set
        {
            if (Instance.status != value)
            {
                Instance.status = value;
                DirtyEditor();
            }
        }
    }

    public static bool Xfbml
    {
        get { return Instance.xfbml; }
        set
        {
            if (Instance.xfbml != value)
            {
                Instance.xfbml = value;
                DirtyEditor();
            }
        }
    }

    public static string IosURLSuffix
    {
        get { return Instance.iosURLSuffix; }
        set 
        {
            if (Instance.iosURLSuffix != value)
            {
                Instance.iosURLSuffix = value;
                DirtyEditor ();
            }
        }
    }

    public static string ChannelUrl
    {
        get { return "/channel.html"; }
    }

    public static bool FrictionlessRequests
    {
        get { return Instance.frictionlessRequests; }
        set
        {
            if (Instance.frictionlessRequests != value)
            {
                Instance.frictionlessRequests = value;
                DirtyEditor();
            }
        }
    }

#if UNITY_EDITOR
/*
    private string testFacebookId = "";
    private string testAccessToken = "";

    public static string TestFacebookId
    {
        get { return Instance.testFacebookId; }
        set
        {
            if (Instance.testFacebookId != value)
            {
                Instance.testFacebookId = value;
                DirtyEditor();
            }
        }
    }

    public static string TestAccessToken
    {
        get { return Instance.testAccessToken; }
        set
        {
            if (Instance.testAccessToken != value)
            {
                Instance.testAccessToken = value;
                DirtyEditor();
            }
        }
    }*/
#endif

    private static void DirtyEditor()
    {
#if UNITY_EDITOR
        EditorUtility.SetDirty(Instance);
#endif
    }

    #endregion
}
