using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace VK {
    class IOSVK : AbstractVK, IVK {
#if UNITY_IOS && !UNITY_EDITOR
        [DllImport ("__Internal")] private static extern void iosVKInit(string appId);
        [DllImport ("__Internal")] private static extern void iosVKLogin(string scope);
        [DllImport ("__Internal")] private static extern void iosVKLogout();
		[DllImport ("__Internal")] private static extern void iosVKShareDialog(string title, string text, string url, string imagePath);
#else
        void iosVKInit(string appId) {}
        void iosVKLogin(string scope) {}
        void iosVKLogout() {}
		void iosVKShareDialog(string title, string text, string url, string imagePath) {}
#endif

        private System.Action externalInitDelegate;
		private System.Action<bool> externalLoginDelegate;

        #region Init
        protected override void OnAwake() {
        }

		public override void Init(System.Action onInitComplete, string appId) {
			externalInitDelegate = onInitComplete;
			iosVKInit(appId);
        }
        #endregion

		public override void Login(string scope = "", System.Action<bool> callback = null) {
			externalLoginDelegate = callback;
            iosVKLogin(scope);
        }

		public override void Logout() {
            iosVKLogout();
            isLoggedIn = false;
        }

		public override bool ShareDialogSupported() {
			return Application.platform == RuntimePlatform.IPhonePlayer;
		}

		public override bool LogoutSupported() {
			return true;
		}

		public override bool PaymentsSupported() {
			return false;
		}

		public override bool InviteBoxSupported() {
			return false;
		}
		
		public override bool OrderBoxSupported() {
			return false;
		}

		public override void ShowOrderBox(OrderType type, int votes, int offerId, bool currency, string item, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail) {
			if (onFail != null)
				onFail();
		}

		public override void ShowInviteBox() {
		}
		
		public override void ShareDialog(string title, string text, string url, string imagePath) {
			iosVKShareDialog(title, text, url, imagePath);
		}

        private void OnInitComplete(string msg) {
            this.isInitialized = true;
            if (!string.IsNullOrEmpty(msg))
                OnLogin(msg);

			externalInitDelegate();
        }

        public void OnLogin(string msg) {
			if (string.IsNullOrEmpty(msg)) {
				// cancelled
				if (externalLoginDelegate != null)
					externalLoginDelegate(false);
				return;
			}
			var parameters = JSON.JsonDecode(msg);
            if (parameters.ContainsKey ("user_id"))
                isLoggedIn = true;

            //pull userId, access token and expiration time out of the response
            ParseLoginDict (parameters);

			if (externalLoginDelegate != null)
				externalLoginDelegate(true);
        }

		public void OnRenewToken(string msg) {
			if (string.IsNullOrEmpty(msg)) {
				// error
				return;
			}
			var parameters = JSON.JsonDecode(msg);
			isLoggedIn = parameters.ContainsKey("user_id");

			//pull userId, access token and expiration time out of the response
			ParseLoginDict (parameters);
		}

		public void OnLogout() {
			accessToken = null;
			isLoggedIn = false;
		}
    }
}
