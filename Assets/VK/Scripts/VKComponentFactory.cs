﻿using System;
using UnityEngine;

namespace VK
{
	public enum IfNotExist
	{
		AddNew,
		ReturnNull
	}

	public class VKComponentFactory
	{
		//
		// Static Fields
		//
		public const string gameObjectName = "UnityVKSDKPlugin";
		
		//
		// Static Properties
		//
		private static GameObject vkGameObject;
		private static GameObject VKGameObject
		{
			get
			{
				if (VKComponentFactory.vkGameObject == null)
					VKComponentFactory.vkGameObject = new GameObject ("UnityVKSDKPlugin");
				return VKComponentFactory.vkGameObject;
			}
		}
		
		//
		// Static Methods
		//
		public static T AddComponent<T> () where T : MonoBehaviour
		{
			return VKComponentFactory.VKGameObject.AddComponent<T> ();
		}
		
		public static T GetComponent<T> (IfNotExist ifNotExist = IfNotExist.AddNew) where T : MonoBehaviour
		{
			GameObject gameObject = VKComponentFactory.VKGameObject;
			T t = gameObject.GetComponent<T> ();
			if (t == null && ifNotExist == IfNotExist.AddNew)
			{
				t = gameObject.AddComponent<T> ();
			}
			return t;
		}
	}
}
