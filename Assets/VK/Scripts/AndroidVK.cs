﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VK {
	class AndroidVK : AbstractVK, IVK {
		private const string AndroidJavaVKClass = "com.amaya.VK";

#if UNITY_ANDROID
		private AndroidJavaObject vkJava;
		private AndroidJavaObject VK {
			get {
				if (vkJava == null) {
					AndroidJavaClass vkClass = new AndroidJavaClass(AndroidJavaVKClass);
					if (vkClass == null)
						throw new MissingReferenceException(string.Format("AndroidVK failed to load {0} class", AndroidJavaVKClass));

					vkJava = vkClass.CallStatic<AndroidJavaObject>("GetInstance");
				}
				return vkJava;
			}
		}
#endif
		private void CallFB(string method, string param) {
			#if UNITY_ANDROID
			VK.Call(method, param);
			#else
			Debug.LogError("Using Android when not on an Android build! Doesn't Work!");
			#endif
		}

		protected override void OnAwake() {
			#if UNITY_ANDROID && DEBUG
			AndroidJNIHelper.debug = true;
			#endif
			#if UNITY_ANDROID
			VK.Call("OnCreate");
			#endif
		}

		private System.Action onInitComplete = null;
		public override void Init(System.Action onInitComplete, string appId) {
			if (string.IsNullOrEmpty(appId))
				throw new System.Exception("appId cannot be null or empty!");
			this.onInitComplete = onInitComplete;

			CallFB("Initialize", appId);
		}

		public void OnInitComplete(string message) {
			isInitialized = true;
			OnLogin(message);
			if (onInitComplete != null)
				onInitComplete();
		}

		private System.Action<bool> onLogin = null;
		public override void Login(string scope = "", System.Action<bool> callback = null) {
			this.onLogin = callback;
			CallFB("Login", scope);
		}

		public void OnLogin(string msg) {
			if (string.IsNullOrEmpty(msg)) {
				// error
				return;
			}
			var parameters = JSON.JsonDecode(msg);
			if (parameters.ContainsKey("user_id"))
				isLoggedIn = true;

			//pull userId, access token and expiration time out of the response
			ParseLoginDict (parameters);
			if (onLogin != null)
				onLogin(true);
		}

		public override void Logout() {
			#if UNITY_ANDROID
			VK.Call("Logout");
			#endif
		}

		public void OnLogout(string message) {
			isLoggedIn = false;
			userId = "";
			accessToken = "";
		}

		public override bool ShareDialogSupported() {
			#if UNITY_ANDROID
			return VK.Call<bool>("ShareDialogSupported");
			#else
			return false;
			#endif
		}

		public override bool LogoutSupported() {
			return true;
		}

		public override bool PaymentsSupported() {
			return false;
		}

		public override bool InviteBoxSupported() {
			return false;
		}

		public override bool OrderBoxSupported() {
			return false;
		}

		public override void ShowOrderBox(OrderType type, int votes, int offerId, bool currency, string item, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail) {
			if (onFail != null)
				onFail();
		}

		public override void ShowInviteBox() {
		}
		
		public override void ShareDialog(string title, string text, string url, string imagePath) {
			#if UNITY_ANDROID
			VK.Call("ShareDialog", title, text, url, imagePath);
			#endif
		}

		public void OnRenewToken(string msg) {
			if (string.IsNullOrEmpty(msg)) {
				// error
				return;
			}
			var parameters = JSON.JsonDecode(msg);
			isLoggedIn = parameters.ContainsKey("user_id");

			//pull userId, access token and expiration time out of the response
			ParseLoginDict (parameters);
		}

#if UNITY_ANDROID
		void OnApplicationPause(bool paused) {
			if (!paused)
				VK.Call("OnResume");
		}

		void OnDestroy() {
			VK.Call("OnDestroy");
		}
#endif
	}
}
