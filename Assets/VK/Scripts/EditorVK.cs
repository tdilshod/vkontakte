﻿#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEditor;

namespace VK {
	class EditorVK : AbstractVK, IVK {
		private string loginScope = "";
		private bool loginInitiated = false;
		private System.Action externalInitDelegate;
		private System.Action<bool> loginCallback;

		#region Init
		protected override void OnAwake() {
		}

		public override void Init(System.Action onInitComplete, string appId) {
			onInitComplete();
		}
		#endregion

		public override void Login(string scope = "", System.Action<bool> callback = null) {
			loginCallback = callback;
			loginScope = scope;
			windowUrl = "";
			loginInitiated = true;
		}

		public override void Logout() {
			accessToken = null;
			UserId = null;
			isLoggedIn = false;
		}

		public override bool ShareDialogSupported() {
			return false;
		}

		public override bool LogoutSupported() {
			return true;
		}

		public override bool PaymentsSupported() {
			return false;
		}

		public override bool InviteBoxSupported() {
			return false;
		}
		
		public override bool OrderBoxSupported() {
			return false;
		}
		
		public override void ShowOrderBox(OrderType type, int votes, int offerId, bool currency, string item, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail) {
			if (onFail != null)
				onFail();
		}

		public override void ShowInviteBox() {
		}
		
		public override void ShareDialog(string title, string text, string url, string imagePath) {
		}

		#region Login GUI
		private const float windowWidth = 492;
		private float windowHeight = 200;
		private Rect windowRect;
		private string windowUrl;

		void OnGUI() {
			if (!loginInitiated)
				return;

			var windowTop = Screen.height / 2 - windowHeight / 2;
			var windowLeft = Screen.width / 2 - windowWidth / 2;
			GUI.ModalWindow(GetHashCode(), new Rect(windowLeft, windowTop, windowWidth, windowHeight), OnGUIDialog, "Unity Editor VK Login");
		}

		void OnGUIDialog(int id) {
			GUILayout.Space(10);
			GUILayout.BeginHorizontal();
			GUILayout.BeginVertical();
			GUILayout.Space(10);
			GUILayout.Label("URL:");
			GUILayout.EndVertical();
			windowUrl = GUILayout.TextField(windowUrl, GUI.skin.textArea, GUILayout.MinWidth(400));
			GUILayout.EndHorizontal();
			GUILayout.Space(20);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Find Access Token")) {
				Application.OpenURL("https://oauth.vk.com/authorize?client_id=" + VKSettings.AppId + "&scope=" + loginScope + "&response_type=token&v=5.28");
			}
			GUILayout.FlexibleSpace();
			var loginLabel = new GUIContent("Login");
			var buttonRect = GUILayoutUtility.GetRect(loginLabel, GUI.skin.button);
			if (GUI.Button(buttonRect, loginLabel)) {
				if (windowUrl.IndexOf('#') > 0) {
					string queryString = windowUrl.Substring(windowUrl.IndexOf('#') + 1);
					foreach (string parameter in queryString.Split('&')) {
						string[] ps = parameter.Split('=');
						if (ps[0] == "access_token")
							accessToken = ps[1];
						else if (ps[0] == "user_id")
							UserId = ps[1];
					};
					if (!string.IsNullOrEmpty(accessToken) && !string.IsNullOrEmpty(UserId)) {
						isLoggedIn = true;
						loginCallback(true);
						loginInitiated = false;
					}
				}
			}
			GUI.enabled = true;
			var cancelLabel = new GUIContent("Cancel");
			var cancelButtonRect = GUILayoutUtility.GetRect(cancelLabel, GUI.skin.button);
			if (GUI.Button(cancelButtonRect, cancelLabel)) {
				loginCallback(false);
				loginInitiated = false;
			}
			GUILayout.EndHorizontal();
		}
		#endregion
	}
}
#endif
