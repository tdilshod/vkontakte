﻿using VK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class VKontakte : ScriptableObject
{
	public static System.Action OnInitComplete;

	private static bool isInitCalled = false;
	private static string appId;
	private static bool cookie;
	private static bool logging;
	private static bool status;
	private static bool xfbml;
	private static bool frictionlessRequests;

	private static IVK vk;
	static IVK VKImpl {
		get {
			if (vk == null)
				throw new NullReferenceException("Facebook object is not yet loaded.  Did you call FB.Init()?");
			return vk;
		}
	}

	public static string AppId {
		get {
			// appId might be different from VKSettings.AppId
			// if using the programmatic version of VK.Init()
			return appId;
		}
	}

	public static string UserId {
		get {
			return (vk != null) ? vk.UserId : "";
		}
	}

	public static string AccessToken {
		get {
			return (vk != null) ? vk.AccessToken : "";
		}
	}
	
	public static DateTime AccessTokenExpiresAt {
		get {
			return (vk != null) ? vk.AccessTokenExpiresAt : DateTime.MinValue;
		}
	}
	
	public static bool IsLoggedIn {
		get {
			return (vk != null) && vk.IsLoggedIn;
		}
	}
	
	public static bool IsInitialized {
		get {
			return (vk != null) && vk.IsInitialized;
		}
	}
	
	#region Init
	/**
     * This is the preferred way to call FB.Init().  It will take the facebook app id specified in your
     * "Facebook" => "Edit Settings" menu when it is called.
     *
     * onInitComplete - Delegate is called when FB.Init() finished initializing everything.
     *                  By passing in a delegate you can find out when you can safely call the other methods.
     */
	public static void Init(System.Action onInitComplete) {
		Init(onInitComplete, VKSettings.AppId);
	}
	
	/**
     * If you need a more programmatic way to set the facebook app id and other setting call this function.
     * Useful for a build pipeline that requires no human input.
     */
	public static void Init(System.Action onInitComplete, string appId) {
		VKontakte.appId = appId;
		VKontakte.OnInitComplete = onInitComplete;

		if (!isInitCalled) {
			#if UNITY_EDITOR
			VKontakte.vk = VKComponentFactory.GetComponent<EditorVK>();
			#elif UNITY_IOS
			VKontakte.vk = VKComponentFactory.GetComponent<IOSVK>();
			#elif UNITY_ANDROID
			VKontakte.vk = VKComponentFactory.GetComponent<AndroidVK>();
			#elif UNITY_WEBPLAYER
			VKontakte.vk = VKComponentFactory.GetComponent<WebplayerVK>();
			#else
			throw new NotImplementedException("VK API does not yet support this platform");
			#endif
			OnDllLoaded();
			isInitCalled = true;
			return;
		}

		Debug.LogWarning("VK.Init() has already been called. You only need to call this once and only once.");

		// Init again if possible just in case something bad actually happened.
		if (VKImpl != null)
			OnDllLoaded();
	}

	private static void OnDllLoaded() {
		VKImpl.Init(OnInitComplete, appId);
	}
	#endregion

	public static void Login(string scope = "", System.Action<bool> callback = null) {
		VKImpl.Login(scope, callback);
	}

	public static void Logout() {
		VKImpl.Logout();
	}

	public static bool ShareDialogSupported() {
		return VKImpl.ShareDialogSupported();
	}

	public static bool LogoutSupported() {
		return VKImpl.LogoutSupported();
	}

	public static bool PaymentsSupported() {
		return VKImpl.PaymentsSupported();
	}

	public static bool InviteBoxSupported() {
		return VKImpl.InviteBoxSupported();
	}

	public static bool OrderBoxSupported() {
		return VKImpl.OrderBoxSupported();
	}

	public static void ShowVotesOrderBox(int votes, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail) {
		VKImpl.ShowOrderBox(OrderType.votes, votes, 0, false, "", onSuccess, onCancel, onFail);
	}

	public static void ShowOffersOrderBox(int offerId, bool currency, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail) {
		VKImpl.ShowOrderBox(OrderType.offers, 0, offerId, currency, "", onSuccess, onCancel, onFail);
	}

	public static void ShowItemOrderBox(string item, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail) {
		VKImpl.ShowOrderBox(OrderType.item, 0, 0, false, item, onSuccess, onCancel, onFail);
	}

	public static void ShareDialog(string title, string text, string url, string imagePath) {
		if (ShareDialogSupported())
			VKImpl.ShareDialog(title, text, url, imagePath);
	}

	public static void ShowInviteBox() {
		if (InviteBoxSupported())
			VKImpl.ShowInviteBox();
	}
}
