﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace VK {
	public class Error {
		public int errorCode;
		public string errorMessage;
	}

	public class Friend {
		public int id;
		public string firstName, lastName;
		public string photoUrl;
		public bool online;

		public Friend(Hashtable h) {
			id = (int)h["id"];
			firstName = h["first_name"] as string;
			lastName = h["last_name"] as string;
			online = (int)h["online"] == 1;
			photoUrl = h["photo_100"] as string;
		}
	}
}

public class VKApi : MonoBehaviour {
	public static bool testMode;
	public static string languageCode; // ru, en, ...
	public static string version = "5.28";

	public static void Like(string type, int itemId) {
		MakeAPIRequest("likes.add", new Hashtable() {
			{"type", type},
			{"owner_id", VKontakte.UserId},
			{"item_id", itemId}
		});
	}

	public static void GetFriends(System.Action<VK.Friend[], VK.Error> callback) {
		MakeAPIRequest("friends.get", new Hashtable() {
			{"order", "name"},
			{"fields", "photo_100"}
		}, (result) => {
			VK.Error error = GetError(result);
			if (error != null)
				callback(null, error);
			else {
				result = (Hashtable)result["response"];
				List<VK.Friend> friends = new List<VK.Friend>();
				foreach (Hashtable h in (ArrayList)result["items"])
					friends.Add(new VK.Friend(h));
				callback(friends.ToArray(), null);
			}
		});
	}

	public static void GetAppFriends(System.Action<VK.Friend[], VK.Error> callback) {
		MakeAPIRequest("friends.getAppUsers", new Hashtable() {
			{"order", "name"},
			{"fields", "photo_100"}
		}, (result) => {
			VK.Error error = GetError(result);
			if (error != null)
				callback(null, error);
			else {
				if (((ArrayList)result["response"]).Count > 0) {
					GetFriends((friends, err) => {
						if (err != null)
							callback(null, err);
						else {
							Dictionary<int, VK.Friend> map = new Dictionary<int, VK.Friend>();
							foreach (var f in friends)
								map[f.id] = f;

							List<VK.Friend> appFriends = new List<VK.Friend>(friends.Length);
							foreach (int h in (ArrayList)result["response"]) {
								VK.Friend f;
								if (map.TryGetValue(h, out f))
									appFriends.Add(f);
							}
							callback(appFriends.ToArray(), null);
						}
					});
				} else
					callback(new VK.Friend[0], null);
			}
		});
	}

	public static void AppSendInviteRequest(int userId, string text, System.Action<int, VK.Error> callback) {
		MakeAPIRequest("apps.sendRequest", new Hashtable() {
			{"user_id", userId},
			{"text", text},
			{"type", "invite"},
			{"key", "whatthefuckisthis"},
			{"name", "ShitKnowsWhatIsThis"},
			//{"separate", "0"},
		}, (result) => {
			VK.Error error = GetError(result);
			if (error != null)
				callback(0, error);
			else if (result["response"] is int)
				callback((int)result["response"], null);
			else if (result["response"] is ArrayList)
				callback((int)((ArrayList)result["response"])[0], null);
			else
				callback(0, null);
		});
	}

	public static void SendBatchInviteRequests(int[] userIds, string text, System.Action<VK.Error> callback) {
		string code = "var userIds = " + VK.JSON.JsonEncode(userIds) + "; var text = " + VK.JSON.JsonEncode(text) + ";\n";
		code+= "var result = [], i = 0;\n";
		code+= "while (i != userIds.length) {\n";
		code+= "result.push(API.apps.sendRequest({user_id: userIds[i], text: text, type: \"invite\", name: \"group\"})); i = i + 1;\n";
		code+= "}; return result;";
		Execute(code, (result, error) => {
			if (callback != null)
				callback(error);
		});
	}

	public static void SendMessageToFriend(int[] friendIds, string message) {
		Hashtable h = new Hashtable() {
			{"message", message}
		};
		if (friendIds.Length == 1)
			h["user_id"] = friendIds[0];
		else
			h["user_ids"] = string.Join(",", System.Array.ConvertAll(friendIds, (f) => f.ToString()));
		MakeAPIRequest("messages.send", h, (result) => {
		});
	}

	/*
	 * Storage
	 */
	public static void StorageGetKeys(bool global, int userId, System.Action<string[], VK.Error> callback, int offset=0, int count=100)
	{
		MakeAPIRequest("storage.getKeys", new Hashtable() {
			{"user_id", global ? 0 : userId},
			{"global", global ? 1 : 0},
			{"offset", offset},
			{"count", count},
		}, (result) => {
			VK.Error error = GetError(result);
			if (error != null)
				callback(null, error);
			else {
				ArrayList arraylist = (ArrayList)result["response"];
				string[] keys = new string[arraylist.Count];
				for (int i=0; i<keys.Length; i++)
					keys[i] = arraylist[i] as string;
				callback(keys, null);
			}
		});
	}

	public static void StorageSet(bool global, int userId, string key, string value, System.Action<VK.Error> callback=null)
	{
		if (string.IsNullOrEmpty(key) || key.Length > 100)
			throw new System.Exception("key length must be within 1 and 100");
		if (string.IsNullOrEmpty(value) || value.Length > 4096)
			throw new System.Exception("key length must be within 1 and 4096");
		MakeAPIRequest("storage.set", new Hashtable() {
			{"user_id", global ? 0 : userId},
			{"global", global ? 1 : 0},
			{"key", key},
			{"value", value},
		}, (result) => {
			if (callback != null)
				callback(GetError(result));
		});
	}

	public static void StorageGet(bool global, int userId, string key, System.Action<string, VK.Error> callback=null)
	{
		MakeAPIRequest("storage.get", new Hashtable() {
			{"user_id", global ? 0 : userId},
			{"global", global ? 1 : 0},
			{"key", key},
		}, (result) => {
			VK.Error error = GetError(result);
			if (error != null)
				callback(null, error);
			else
				callback(result["response"] as string, null);
		});
	}

	public static void StorageGet(bool global, int userId, string[] keys, System.Action<Dictionary<string, string>, VK.Error> callback=null)
	{
		MakeAPIRequest("storage.get", new Hashtable() {
			{"user_id", global ? 0 : userId},
			{"global", global ? 1 : 0},
			{"keys", string.Join(",", keys)},
		}, (result) => {
			VK.Error error = GetError(result);
			if (error != null)
				callback(null, error);
			else {
				ArrayList arraylist = (ArrayList)result["response"];
				Dictionary<string, string> dict = new Dictionary<string, string>(arraylist.Count);
				foreach (Hashtable h in arraylist) {
					if (h == null)
						continue;
					string key = h["key"] as string;
					string value = h["value"] as string;
					if (key != null && dict != null)
						dict[key] = value;
				}
				callback(dict, null);
			}
		});
	}
	
	/*
	 * Post To Wall
	 */
	public static void PostToWall(string message, string url=null) {
		#if UNITY_WEBPLAYER && !UNITY_EDITOR
		Application.ExternalCall("VKPostToWall", message, url);
		#else
		var parameters = new Hashtable() {
			{"message", message},
			{"services", "facebook,twitter"}
		};
		if (!string.IsNullOrEmpty(url))
			parameters["attachments"] = url;
		MakeAPIRequest("wall.post", parameters, (result) => {
		});
		#endif
	}

	/*
	 * Execute VKScript
	 */
	public static void Execute(string vkScript, System.Action<Hashtable, VK.Error> callback)
	{
		MakePostAPIRequest("execute", new Hashtable() {
			{"code", vkScript}
		}, (result) => {
			VK.Error error = GetError(result);
			if (error != null)
				callback(null, error);
			else
				callback(result["response"] as Hashtable, null);
		});
	}

	/*
	 * Private methods
	 */
	static VK.Error GetError(Hashtable result) {
		if (result == null) {
			return new VK.Error() {
				errorCode = -1,
				errorMessage = "Connection error!"
			};
		}
		if (result.ContainsKey("error")) {
			Hashtable h = (Hashtable)result["error"];
			return new VK.Error() {
				errorCode = (int)h["error_code"],
				errorMessage = h["error_msg"] as string
			};
		}
		return null;
	}

	static void MakeAPIRequest(string methodName, Hashtable attributes, System.Action<Hashtable> onComplete=null) {
		attributes["access_token"] = VKontakte.AccessToken;
		if (!string.IsNullOrEmpty(version))
			attributes["v"] = version;
		if (!string.IsNullOrEmpty(languageCode))
			attributes["lang"] = languageCode;
		if (testMode)
			attributes["test_mode"] = "1";

		string[] ps = new string[attributes.Count];
		int i = 0;
		foreach (DictionaryEntry e in attributes)
			ps[i++] = WWW.EscapeURL(e.Key.ToString()) + "=" + WWW.EscapeURL(e.Value.ToString());

		string parameters = string.Join("&", ps);

		string url = "https://api.vk.com/method/" + methodName + "?" + parameters;
		VK.VKComponentFactory.GetComponent<VKApi>().StartCoroutine(MakeRequest(url, onComplete));
	}

	static void MakePostAPIRequest(string methodName, Hashtable attributes, System.Action<Hashtable> onComplete=null) {
		attributes["access_token"] = VKontakte.AccessToken;
		if (!string.IsNullOrEmpty(version))
			attributes["v"] = version;
		if (!string.IsNullOrEmpty(languageCode))
			attributes["lang"] = languageCode;
		if (testMode)
			attributes["test_mode"] = "1";
		
		string[] ps = new string[attributes.Count];
		int i = 0;
		foreach (DictionaryEntry e in attributes)
			ps[i++] = WWW.EscapeURL(e.Key.ToString()) + "=" + WWW.EscapeURL(e.Value.ToString());
		
		string parameters = string.Join("&", ps);
		byte[] postData = System.Text.UTF8Encoding.UTF8.GetBytes(parameters);

		string url = "https://api.vk.com/method/" + methodName;
		VK.VKComponentFactory.GetComponent<VKApi>().StartCoroutine(MakePostRequest(url, postData, onComplete));
	}

	static IEnumerator MakeRequest(string url, System.Action<Hashtable> callback) {
		WWW www = new WWW(url);
		yield return www;
		if (callback != null) {
			if (www.error == null) {
				callback(VK.JSON.JsonDecode(www.text));
			} else {
				callback(null);
			}
		}
	}

	static IEnumerator MakePostRequest(string url, byte[] postData, System.Action<Hashtable> callback) {
		WWW www = new WWW(url, postData);
		yield return www;
		if (callback != null) {
			if (www.error == null) {
				callback(VK.JSON.JsonDecode(www.text));
			} else {
				callback(null);
			}
		}
	}
}
