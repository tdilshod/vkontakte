﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace VK {
	public delegate void VKDelegate(object result);

	public interface IVK {
		//
		// Properties
		//
		string AccessToken { get; }
		DateTime AccessTokenExpiresAt { get; }
		bool IsInitialized { get; }
		bool IsLoggedIn { get; }
		string UserId { get; }

		//
		// Methods
		//
		bool ShareDialogSupported();
		bool LogoutSupported();
		bool PaymentsSupported();
		bool InviteBoxSupported();
		bool OrderBoxSupported();

		void Init(System.Action onInitComplete, string appId);
		void Login(string scope = "", System.Action<bool> callback = null);
		void Logout();
		void ShareDialog(string title, string text, string url, string imagePath);
		void ShowInviteBox();
		void ShowOrderBox(OrderType type, int votes, int offerId, bool currency, string item, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail);
	}
}
