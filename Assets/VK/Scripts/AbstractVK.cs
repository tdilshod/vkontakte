﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VK {
	public enum OrderType {
		votes,
		offers,
		item
	}

	public abstract class AbstractVK : MonoBehaviour, IVK {
		//
		// Fields
		//
		protected string accessToken;
		protected DateTime accessTokenExpiresAt;
		protected bool limitEventUsage;
		protected bool isInitialized;
		protected bool isLoggedIn;
		protected string userId;

		//
		// Properties
		//
		public virtual string AccessToken {
			get {
				return this.accessToken;
			}
			set {
				this.accessToken = value;
			}
		}

		public virtual DateTime AccessTokenExpiresAt {
			get {
				return this.accessTokenExpiresAt;
			}
		}

		public bool IsInitialized {
			get {
				return this.isInitialized;
			}
		}

		public bool IsLoggedIn {
			get {
				return this.isLoggedIn;
			}
		}

		public string UserId {
			get {
				return this.userId;
			}
			set {
				this.userId = value;
			}
		}

		void Awake() {
			OnAwake();
		}

		public abstract bool ShareDialogSupported();
		public abstract bool LogoutSupported();
		public abstract bool PaymentsSupported();
		public abstract bool InviteBoxSupported();
		public abstract bool OrderBoxSupported();

		public abstract void Init(System.Action onInitComplete, string appId);
		public abstract void Login(string scope = "", System.Action<bool> callback = null);
		public abstract void Logout();
		public abstract void ShareDialog(string title, string text, string url, string imagePath);
		public abstract void ShowInviteBox();
		public abstract void ShowOrderBox(OrderType type, int votes, int offerId, bool currency, string item, System.Action<int> onSuccess, System.Action onCancel, System.Action onFail);
		protected abstract void OnAwake();

		public void ParseLoginDict(Hashtable parameters) {
			if (parameters.ContainsKey ("user_id"))
				userId = (string)parameters ["user_id"];
			
			if (parameters.ContainsKey ("access_token"))
				accessToken = (string)parameters ["access_token"];
		}
	}
}
