package com.amaya;

import android.content.Intent;
import android.os.Bundle;
import com.unity3d.player.UnityPlayerActivity;
import com.vk.sdk.VKUIHelper;

/**
 * Created by dilshod on 2/2/15.
 */
public class VKActivity extends UnityPlayerActivity {
    @Override
    protected void onCreate(Bundle var1) {
        super.onCreate(var1);
        VKUIHelper.onCreate(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }
}
