package com.amaya;

import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.unity3d.player.UnityPlayer;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;
import com.vk.sdk.dialogs.VKCaptchaDialog;
import com.vk.sdk.dialogs.VKShareDialog;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by dilshod on 2/2/15.
 */
public class VK extends VKSdkListener {
    static final String vkObjName = "UnityVKSDKPlugin";

    public void OnCreate() {
        VKUIHelper.onCreate(UnityPlayer.currentActivity);
    }

    public void OnResume() {
        VKUIHelper.onResume(UnityPlayer.currentActivity);
    }

    public void OnDestroy() {
        VKUIHelper.onDestroy(UnityPlayer.currentActivity);
    }

    public static VK GetInstance() {
        return new VK();
    }

    public void Initialize(String appId) {
        VKSdk.initialize(this, appId);
        String result = "";
        if (VKSdk.wakeUpSession()) {
            VKAccessToken accessToken = VKSdk.getAccessToken();
            if (accessToken != null) {
                JSONObject jo = new JSONObject();
                try {
                    jo.put("user_id", accessToken.userId);
                    jo.put("access_token", accessToken.accessToken);
                    jo.put("expiration_timestamp", accessToken.expiresIn);
                    result = jo.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        UnityPlayer.UnitySendMessage(vkObjName, "OnInitComplete", result);
    }

    public void Login(String scope) {
        String[] scopeArray = scope.split(",");
        VKSdk.authorize(scopeArray, true, false);
    }

    public void Logout() {
        VKSdk.logout();
        UnityPlayer.UnitySendMessage(vkObjName, "OnLogout", "");
    }

    public boolean ShareDialogSupported() {
        return android.os.Build.VERSION.SDK_INT >= 11;
    }

    public void ShareDialog(String title, String text, String url, String imagePath) throws IOException {
        VKShareDialog shareDialog = new VKShareDialog().setShareDialogListener(new VKShareDialog.VKShareDialogListener() {
            public void onVkShareComplete(int postId) {
            }

            public void onVkShareCancel() {
            }
        });

        if (text != null)
            shareDialog.setText(text);
        if (imagePath != null) {
            Bitmap bitmap = null;
            if (imagePath.startsWith("jar:file://")) {
                InputStream is = getClass().getResourceAsStream(imagePath.substring(imagePath.indexOf("!/") + 1));
                if (is != null) {
                    bitmap = BitmapFactory.decodeStream(is);
                    is.close();
                }
            } else {
                bitmap = BitmapFactory.decodeFile(imagePath);
            }

            if (bitmap != null) {
                shareDialog.setAttachmentImages(new VKUploadImage[]{
                        new VKUploadImage(bitmap, VKImageParameters.jpgImage(0.9f))
                });
            }
        }
        if (url != null)
            shareDialog.setAttachmentLink(title, url);

        FragmentManager fm =  UnityPlayer.currentActivity.getFragmentManager();
        shareDialog.show(fm, "VK_SHARE_DIALOG");
    }

    public void onCaptchaError(VKError captchaError) {
        new VKCaptchaDialog(captchaError).show();
    }

    public void onTokenExpired(VKAccessToken expiredToken) {
        UnityPlayer.UnitySendMessage(vkObjName, "OnLogout", "");
    }

    public void onAccessDenied(VKError authorizationError) {
        UnityPlayer.UnitySendMessage(vkObjName, "OnLogin", "");
    }

    public void onReceiveNewToken(VKAccessToken accessToken) {
        JSONObject jo = new JSONObject();
        try {
            jo.put("user_id", accessToken.userId);
            jo.put("access_token", accessToken.accessToken);
            jo.put("expiration_timestamp", accessToken.expiresIn);
            UnityPlayer.UnitySendMessage(vkObjName, "OnLogin", jo.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onAcceptUserToken(VKAccessToken accessToken) {
        JSONObject jo = new JSONObject();
        try {
            jo.put("user_id", accessToken.userId);
            jo.put("access_token", accessToken.accessToken);
            jo.put("expiration_timestamp", accessToken.expiresIn);
            UnityPlayer.UnitySendMessage(vkObjName, "OnRenewToken", jo.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onRenewAccessToken(VKAccessToken accessToken) {
        JSONObject jo = new JSONObject();
        try {
            jo.put("user_id", accessToken.userId);
            jo.put("access_token", accessToken.accessToken);
            jo.put("expiration_timestamp", accessToken.expiresIn);
            UnityPlayer.UnitySendMessage(vkObjName, "OnRenewToken", jo.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
