
from google.appengine.api import memcache
from google.appengine.ext import ndb

class Order(ndb.Model):
    app_id = ndb.IntegerProperty()
    user_id = ndb.IntegerProperty()
    item = ndb.StringProperty()
    item_id = ndb.StringProperty()
    item_price = ndb.StringProperty()
    order_id = ndb.IntegerProperty(indexed=True)
