#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2, json, md5, string
from models import *

SECRET_KEY = "GYdnIQpi6EiSXj1RWSbO"

class MainHandler(webapp2.RequestHandler):
    def getparam(self, key, default_value=None):
        value = self.request.get(key)
        if value and len(value) > 0:
            return str(value)
        return default_value

    def verifySig(self):
        items = self.request.params.items()
        items = filter(lambda i: i[0] != "sig", items)
        data = string.join(map(lambda i: i[0] + "=" + i[1], items), "")
        sigVerify = md5.md5(data + SECRET_KEY).hexdigest()

        print("Parameters: " + string.join(map(lambda i: i[0] + "=" + i[1], items), "&"))
        #print("SigVerify: " + sigVerify)
        #print("Sig: " + str(self.getparam("sig")))
        return sigVerify == self.getparam("sig")

    def post(self):
        return self.get()

    def get(self):
        if not self.verifySig():
            # access denied
            json.dump({"error" : {"error_code" : 10, "error_msg" : "Invalid signature!", "critical" : True}}, self.response.out)
            return

        notification_type = self.getparam('notification_type')
        app_id = self.getparam('app_id')
        user_id = self.getparam('user_id')
        receiver_id = self.getparam('receiver_id')
        order_id = self.getparam('order_id')
        if order_id:
            order_id = int(order_id)

        result = {}
        if notification_type == "get_item" or notification_type == "get_item_test":
            # information about item
            item = self.getparam('item')
            lang = self.getparam('lang')
            result = {"response" : {"title" : "300 coins", "photo_url" : "http://somesite.ru/images/coins.jpg", "price" : 10}}

        elif notification_type == "order_status_change_test" or notification_type == "order_status_change":
            # order status change
            status = self.getparam('status')
            item = self.getparam('item')
            item_id = self.getparam('item_id')
            item_title = self.getparam('item_title')
            item_photo_url = self.getparam('item_photo_url')
            item_price = self.getparam('item_price')

            if status == "chargeable":
                order = Order()
                order.app_id = int(app_id)
                order.user_id = int(user_id)
                order.item = item
                order.item_id = item_id
                order.item_price = item_price
                order.order_id = order_id
                order.put()

                result = {"response" : {"order_id" : order_id, "app_order_id" : order_id}}
            else:
                result = {"error" : {"error_code" : 1, "error_msg" : "Invalid Parameters", "critical" : True}}

        json.dump(result, self.response.out)

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
